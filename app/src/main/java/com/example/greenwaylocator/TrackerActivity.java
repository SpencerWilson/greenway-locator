/*  Greenway Locator
 *  Group 11
 *  TrackerActivity.java
 *  This class is used to calculate and display workout statistics while the user is working out
 * */
package com.example.greenwaylocator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Chronometer;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class TrackerActivity extends AppCompatActivity {
    // Initialize Variables
    private Chronometer chronometer;
    private long pauseOffset;
    private boolean run;

    BottomNavigationView bottomNavigationView;

    TextView weight;
    TextView activity;
    String weightDisplay;
    String activityDisplay;
    TextView calDisplay;
    double funcBurn;
    double valWeight;
    double timeUpdate = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_tracker);

        weight = findViewById(R.id.valueWeight);
        try {
            weightDisplay = getIntent().getExtras().getString("weight");
            weight.setText(weightDisplay);
        } catch (Exception e) {
            weightDisplay = null;
            weight.setText("Weight Needed!");
        }


        activity = findViewById(R.id.valueActivity);
        try {
            activityDisplay = getIntent().getExtras().getString("activity");
            activity.setText(activityDisplay);
        } catch (Exception e) {
            activityDisplay = "Select an activity!";
            activity.setText(activityDisplay);
        }


        calDisplay = findViewById(R.id.dispCalories);

        chronometer = findViewById(R.id.stopwatch);

        bottomNavigationView = findViewById(R.id.nav_bar);
        bottomNavigationView.setSelectedItemId(R.id.nav_tracker);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_greenways:
                        startActivity(new Intent(getApplicationContext()
                                ,MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_health:
                        startActivity(new Intent(getApplicationContext()
                                ,HealthActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_workouts:
                        startActivity(new Intent(getApplicationContext()
                                ,WorkoutActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });
    }
    //Total calories burned = Duration (in minutes) x (MET x 3.5 x Weight in kg) / 200
    public void startWatch (View v)
    {
        if(!run)
        {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            run = true;
            try {
                valWeight = Double.parseDouble(weightDisplay);
            } catch (Exception e) {
                AlertDialog alt = new AlertDialog.Builder(TrackerActivity.this)
                        .setTitle("Weight Needed on Health Tab!")
                        .setMessage("Please input your weight on the Health Tab")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(getApplicationContext()
                                ,HealthActivity.class));
                            }
                        })
                        .create();
                alt.show();
            }


            chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {
                    timeUpdate++;

                        switch (activityDisplay) {
                            case "Running":
                                funcBurn += (timeUpdate / 60) * (9.8 * 3.5 * valWeight) / 200;
                                String dispBurnRun = String.format("%.2f", funcBurn);
                                calDisplay.setText(dispBurnRun);
                                timeUpdate = 0;
                                break;
                            case "Walking":
                                funcBurn += (timeUpdate / 60) * (2.9 * 3.5 * valWeight) / 200;
                                String dispBurnWalk = String.format("%.2f", funcBurn);
                                calDisplay.setText(dispBurnWalk);
                                timeUpdate = 0;
                                break;
                            case "Biking":
                                funcBurn += (timeUpdate / 60) * (8 * 3.5 * valWeight) / 200;
                                String dispBurnBike = String.format("%.2f", funcBurn);
                                calDisplay.setText(dispBurnBike);
                                timeUpdate = 0;
                                break;
                        }


                }
            });

        }
    }
    // Pauses clock and calorie calculation
    public void pauseWatch (View v)
    {
        if(run)
        {
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            run = false;
        }
    }
    // Resets clock and calories burned
    public void resetWatch (View v)
    {
        chronometer.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
        funcBurn = 0;
        calDisplay.setText("0");
    }
}